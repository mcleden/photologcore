import XCTest

import PhotoLogCoreTests

var tests = [XCTestCaseEntry]()
tests += PhotoLogCoreTests.allTests()
XCTMain(tests)
