# PhotoLogCore

A Swift Package to provide a shared model to save photo information from an iOS app to a Vapor 4 server.

This has been moved to a remote repository to avoid local importation issues.
