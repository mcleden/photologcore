//
//  PhotoLog.swift
//  Swift Package used by sol-AR (app) and lun-AR (server)
//
//  Created by Dane Muldoon on 19/4/20.
//  Copyright © 2020 Dane Muldoon. All rights reserved.
//

import Foundation

//  For each AR photo taken upload data to server about the model, the location / time and the capture device

public final class PhotoLog: Codable {
    public var photoID: UUID?
    public var manufacturer: String
    public var modelSearchName: String
    public var takenDate: String
    public var latitude: String
    public var longitude: String
    public var deviceType: String
    public var deviceOS: String

    init(photoID: UUID?,
         manufacturer: String,
         model: String,
         takenDate: String,
         latitude: String,
         longitude: String,
         deviceType: String,
         deviceOS: String
        ) {
        self.photoID = photoID
        self.manufacturer = manufacturer
        self.modelSearchName = model
        self.takenDate = takenDate
        self.latitude = latitude
        self.longitude = longitude
        self.deviceType = deviceType
        self.deviceOS = deviceOS
    }
}
